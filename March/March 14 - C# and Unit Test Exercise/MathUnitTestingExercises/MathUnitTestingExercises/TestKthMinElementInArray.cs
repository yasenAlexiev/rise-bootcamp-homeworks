﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathUnitTestingExercises
{
    [TestClass]
    public class TestKthMinElementInArray
    {
        [TestMethod]
        public void TestMinElementInArrayGivenZeroLength()
        {
            //Arrange
            MathFunctions.MathFunctions math = new MathFunctions.MathFunctions();
            int[] testArr = { };
            //Act
            try
            {
                math.KthMinElementInArray(1, testArr);
            }
            //Assert
            catch
            {
                Assert.IsTrue(true);
            }
        }
        [TestMethod]
        public void TestMinElementInArrayGivenZeroKthElement()
        {
            //Arrange
            MathFunctions.MathFunctions math = new MathFunctions.MathFunctions();
            int[] testArr = {1, 2, 3, 4, 5};
            //Act
            try
            {
                math.KthMinElementInArray(0, testArr);
            }
            //Assert
            catch
            {
                Assert.IsTrue(true);
            }
        }
        [TestMethod]
        public void TestMinElementInArrayGivenKLargerThanArr()
        {
            //Arrange
            MathFunctions.MathFunctions math = new MathFunctions.MathFunctions();
            int[] testArr = {1, 2, 3, 4, 5};
            //Act
            try
            {
                math.KthMinElementInArray(10, testArr);
            }
            //Assert
            catch
            {
                Assert.IsTrue(true);
            }
        }
        [TestMethod]
        public void TestKthMinElementInArrayGivenArray1()
        {
            //Arrange
            MathFunctions.MathFunctions math = new MathFunctions.MathFunctions();
            int[] testArr = { 1, 2, 3, 4, 5 };
            bool result;

            //Act
            if (math.KthMinElementInArray(3, testArr) == 3) result = true;
            else result = false;

            //Assert
            Assert.IsTrue(result);
        }
        [TestMethod]
        public void TestKthMinElementInArrayGivenArray2()
        {
            //Arrange
            MathFunctions.MathFunctions math = new MathFunctions.MathFunctions();
            int[] testArr = { 25, 362, 30, 4, 52 };
            bool result;

            //Act
            if (math.KthMinElementInArray(3, testArr) == 30) result = true;
            else result = false;

            //Assert
            Assert.IsTrue(result);
        }
        [TestMethod]
        public void TestKthMinElementInArrayGivenArray3()
        {
            //Arrange
            MathFunctions.MathFunctions math = new MathFunctions.MathFunctions();
            int[] testArr = { 10, 9, 8, 7, 6, 5, 4, 3, 2, 1 };
            bool result;

            //Act
            if (math.KthMinElementInArray(3, testArr) == 3) result = true;
            else result = false;

            //Assert
            Assert.IsTrue(result);
        }
        [TestMethod]
        public void TestKthMinElementInArrayGivenFirstMin()
        {
            //Arrange
            MathFunctions.MathFunctions math = new MathFunctions.MathFunctions();
            int[] testArr = { 1, 2, 3, 4, 5 };
            bool result;

            //Act
            if (math.KthMinElementInArray(1, testArr) == 1) result = true;
            else result = false;

            //Assert
            Assert.IsTrue(result);
        }
        [TestMethod]
        public void TestKthMinElementInArrayGivenLastMin()
        {
            //Arrange
            MathFunctions.MathFunctions math = new MathFunctions.MathFunctions();
            int[] testArr = { 1, 2, 3, 4, 5 };
            bool result;

            //Act
            if (math.KthMinElementInArray(5, testArr) == 5) result = true;
            else result = false;

            //Assert
            Assert.IsTrue(result);
        }
    }
}
