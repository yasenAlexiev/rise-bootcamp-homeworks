using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathUnitTestingExercises
{
    [TestClass()]
    public class TestIsOddClass
    {
        [TestMethod]
        public void TestIsOddGivenOddNumbers()
        {
            //Arrange
            MathFunctions.MathFunctions math = new MathFunctions.MathFunctions();
            int testNum = 5;
            bool result;

            //Act
            result = math.IsOdd(testNum);

            //Assert
            Assert.IsTrue(result);
        }
        [TestMethod]
        public void TestIsOddGivenEvenNumbers()
        {
            //Arrange
            MathFunctions.MathFunctions math = new MathFunctions.MathFunctions();
            int testNum = 2;
            bool result;

            //Act
            result = math.IsOdd(testNum);

            //Assert
            Assert.IsFalse(result);
        }
        [TestMethod]
        public void TestIsOddGivenZero()
        {
            //Arrange
            MathFunctions.MathFunctions math = new MathFunctions.MathFunctions();
            int testNum = 0;
            bool result;

            //Act
            result = math.IsOdd(testNum);

            //Assert
            Assert.IsFalse(result);
        }
    }
}