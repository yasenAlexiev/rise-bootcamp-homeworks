using ArraysSortingExercisesProject;
using System;

namespace TestingArraysProject
{
    [TestClass]
    public class TestMergeSortClass
    {
        [TestMethod]
        public void TestSortGivenReverseArray()
        {
            int[] testArray = new int[] { 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 };
            int[] expected = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

            int[] sortedArray = ArraySorting.MergeSort(testArray);

            Assert.IsNotNull(sortedArray);
            CollectionAssert.AreEqual(sortedArray, expected);
        }
        [TestMethod]
        public void TestSortGivenRandomArray()
        {
            int[] testArray = new int[] { 4, 8, 3, 6, 2, 7, 1, 9, 5, 0 };
            int[] expected = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

            int[] sortedArray = ArraySorting.MergeSort(testArray);

            Assert.IsNotNull(sortedArray);
            CollectionAssert.AreEqual(sortedArray, expected);
        }
        [TestMethod]
        public void TestSortGivenSortedArray()
        {
            int[] testArray = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            int[] expected = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

            int[] sortedArray = ArraySorting.MergeSort(testArray);

            Assert.IsNotNull(sortedArray);
            CollectionAssert.AreEqual(sortedArray, expected);
        }
        [TestMethod]
        public void TestSortGivenShortArray()
        {
            int[] testArray = new int[] { 1, 0 };
            int[] expected = new int[] { 0, 1 };

            int[] sortedArray = ArraySorting.MergeSort(testArray);

            Assert.IsNotNull(sortedArray);
            CollectionAssert.AreEqual(sortedArray, expected);
        }
        [TestMethod]
        public void TestSortGivenSingleItemArray()
        {
            int[] testArray = new int[] { 1 };
            int[] expected = new int[] { 1 };

            int[] sortedArray = ArraySorting.MergeSort(testArray);

            Assert.IsNotNull(sortedArray);
            CollectionAssert.AreEqual(sortedArray, expected);
        }
        [TestMethod]
        public void TestSortGivenEmptyArray()
        {
            int[] testArray = new int[] { };
            int[] expected = new int[] { };

            int[] sortedArray = ArraySorting.MergeSort(testArray);

            Assert.IsNotNull(sortedArray);
            CollectionAssert.AreEqual(sortedArray, expected);
        }
        [TestMethod]
        public void TestSortGivenNullArray()
        {
            int[] testArray = null;
            int[] expected = null;

            try
            {
                int[] sortedArray = ArraySorting.MergeSort(testArray);
            }
            catch
            {
                Assert.IsTrue(true);
            }
        }
    }
}